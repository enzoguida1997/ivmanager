import SwiftUI

struct ContentViewTimer: View {
    
    

    @State var timeRemaining = 10
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

    var body: some View {
        Text("\(timeRemaining)")
            .onReceive(timer) { _ in
                if self.timeRemaining > 0 {
                    self.timeRemaining -= 1
                }
            }
        
    }
}

struct ContentViewTimer_preview: PreviewProvider {
    static var previews: some View {
        ContentViewTimer()
    }
}
