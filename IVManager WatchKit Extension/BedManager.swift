//
//  BedManager.swift
//  IVManager WatchKit Extension
//
//  Created by Vincenzo Guida on 14/01/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct BedManager: View {
     var body: some View {

                        List(0..<7){ item in
                            NavigationLink(destination: IVManager()) {
                                HStack{
                                    Text("Bed")
                                    Text("1")
                                }
                            }
                        }
                .navigationBarTitle("Bed Manager")
            }
}

struct BedManager_Previews: PreviewProvider {
    static var previews: some View {
        BedManager()
    }
}
