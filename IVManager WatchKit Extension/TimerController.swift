////
////  TimerController.swift
////  IVManager WatchKit Extension
////
////  Created by Vincenzo Guida on 15/01/2020.
////  Copyright © 2020 Vincenzo Guida. All rights reserved.
//
//
//import WatchKit
//import Foundation
//import SwiftUI
//
//class TimerHostingController: WKHostingController<TimerViewContainer> {
//    override var body: TimerViewContainer {
//        return TimerViewContainer()
//    }
//
//    override func didAppear() {
//        print("didAppear TimerView")
//        userStatus.currentPage = .timer
//    }
//}
//
//struct TimerViewContainer: View {
//    var body: some View {
//        TimerView()
//            .environmentObject(userStatus)
//    }
//}
//
