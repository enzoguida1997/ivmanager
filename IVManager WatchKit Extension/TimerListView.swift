//
//  TimerListView.swift
//  IVManager WatchKit Extension
//
//  Created by Vincenzo Guida on 16/01/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct TimerArray : Identifiable{
    let id: Int
    var time : Int
}


struct TimerListView: View {
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State var arrayTimer = [TimerArray(id: 0, time: 20)]
    var body: some View {
        List(arrayTimer){ item in
                Text("\(item.time)")
                    .onReceive(self.timer) { _ in
                        if self.arrayTimer[item.id].time > 0 {
                            self.arrayTimer[item.id].time -= 1
                    }
                }
            
        }
    }
}

struct TimerListView_Previews: PreviewProvider {
    static var previews: some View {
        TimerListView()
    }
}
