//
//  IVManager.swift
//  IVManager WatchKit Extension
//
//  Created by Vincenzo Guida on 14/01/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct IVManager: View {
    var room = "Loading"
    var bed = "Loading"
    @State private var selectedDropsMin = 0
    
    var listAdd = TimerListView()
    
    var drops = [30,50,60]
    var body: some View {
        
        VStack{
            Text("Room \(room) Bed \(bed)")
                .padding(10)
            
            Picker(selection:  $selectedDropsMin , label: Text("DROPS/MIN")) {
                ForEach(0 ..< drops.count) {
                    Text(String(self.drops[$0]))
                }
            }
            .padding(10)
            
        
            
//            Button(action: {
//                let time = TimerArray(time: self.drops[self.selectedDropsMin])
//                self.listAdd.times.items.append(time)}) {
//                Text(/*@START_MENU_TOKEN@*/"Button"/*@END_MENU_TOKEN@*/)
//            }
            

        }
        .scaledToFit()
        .navigationBarTitle("IV Timer")
    }
    
//    private func addRow() {
//        self.listAdd.$arrayTimer.append()
//    }

}


struct IVManager_Previews: PreviewProvider {
    static var previews: some View {
        IVManager()
    }
}
