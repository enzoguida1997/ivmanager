//
//  HostingController.swift
//  IVManager WatchKit Extension
//
//  Created by Vincenzo Guida on 13/01/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
