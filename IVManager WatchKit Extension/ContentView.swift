//
//  ContentView.swift
//  IVManager WatchKit Extension
//
//  Created by Vincenzo Guida on 13/01/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var selectedRoom = 0
    @State private var selectedBed = 0
    
    var room = ["001", "002", "003"]
    var bed = ["1", "2", "3"]
    
    
    
    
    var body: some View {
        
        //                List(0..<7){ item in
        //                    NavigationLink(destination: BedManager()) {
        //                        HStack{
        //                            Text("Room")
        //                            Text("1")
        //                        }
        //                    }
        //                }
        VStack{
            HStack{
                Picker(selection:  $selectedRoom , label: Text("ROOM")) {
                    ForEach(0 ..< room.count) {
                        Text(self.room[$0])
                    }
                }
                Picker(selection:  $selectedBed , label: Text("BED")) {
                    ForEach(0 ..< bed.count) {
                        Text(self.bed[$0])
                    }
                }
                
            }


            HStack{
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                    HStack {
                        Image(systemName: "info.circle")
                        Text("Info")
                    }
                }
                NavigationLink(destination: IVManager(room: room[selectedRoom], bed: bed[selectedBed])){
                    Text("Ok")
                }
                
            }
        }
            
        .navigationBarTitle("IV Manager")
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
